package main

import (
	"net/http"
	"runtime"
	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"
	"github.com/say/apisql2/controllers"
	"github.com/say/apisql2/db"
	//"os/user"
)

//CORSMiddleware ...
func CORSMiddleware() gin.HandlerFunc {

	return func(c *gin.Context) {
		c.Header("Access-Control-Allow-Headers", "access-control-allow-origin, access-control-allow-headers")
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
		c.Writer.Header().Set("Cache-Control", "private, no-cache, no-store, must-revalidate")
		c.Writer.Header().Set("Expires", "-1")
		c.Writer.Header().Set("Pragma", "no-cache")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}

func main() {
	r := gin.Default()
    r.RedirectTrailingSlash=false
	store, _ := sessions.NewRedisStore(10, "tcp", "localhost:6379", "", []byte("secret"))
	r.Use(CORSMiddleware())
	r.Use(sessions.Sessions("gin-boilerplate-session", store))

	db.Init()

	v1 := r.Group("/api/v1")
	{
		exportService := new(controllers.ExportServiceController)
		exportServiceValues := new(controllers.ExportServiceValuesController)
		v1.POST("/exportServicesValues",  exportServiceValues.Add)
		v1.GET("/exportServices",  exportService.All)
		v1.GET("/exportServices/:id", exportService.One)
		v1.GET("/exportServicesValues",  exportServiceValues.All)
		v1.GET("/exportServicesValues/:year",  exportServiceValues.AllYear)
		v1.GET("/exportServicesValues/:year/:month",  exportServiceValues.AllMonth)
		v1.GET("/exportServicesValues/:year/:month/:pred",  exportServiceValues.AllMonthPred)

		/*** START USER ***/
		//user := new(controllers.UserController)
		//
		//v1.POST("/user/signin", user.Signin)
		//v1.POST("/user/signup", user.Signup)
		//v1.GET("/user/signout", user.Signout)

		/*** START Article ***/
		//article := new(controllers.ArticleController)

		//v1.POST("/article", article.Create)
		//v1.GET("/articles", article.All)
		//v1.GET("/article/:id", article.One)
		//v1.PUT("/article/:id", article.Update)
		//v1.DELETE("/article/:id", article.Delete)
	}

	r.LoadHTMLGlob("./public/html/*")

	r.Static("/public", "./public")

	r.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", gin.H{
			"ginBoilerplateVersion": "v0.03",
			"goVersion":             runtime.Version(),
		})
	})

	r.NoRoute(NoRoute)

	r.Run(":9000")
}
func NoRoute (c *gin.Context) {
	c.JSON(404, gin.H{"Code": "404","Message": "Not Found"})
	c.Abort()

}