package models

import (
	"github.com/say/apisql2/db"
	"database/sql"

)

//Article ...
//type Article struct {
//	ID        int64    `db:"id, primarykey, autoincrement" json:"id"`
//	UserID    int64    `db:"user_id" json:"-"`
//	Title     string   `db:"title" json:"title"`
//	Content   string   `db:"content" json:"content"`
//	UpdatedAt int64    `db:"updated_at" json:"updated_at"`
//	CreatedAt int64    `db:"created_at" json:"created_at"`
//	User      *JSONRaw `db:"user" json:"user"`
//}
type ExportService struct {
	Id int64
	IdPred int64
	LongName string
	UserId int64
	SortList int64
	GroupLIst string
	ShortName string
	NodmGroups string
	NodmGroupSort int64
	GroupNumber int64
}

//ArticleModel ...
type ExportServiceModel struct{}

//All ...
func (m ExportServiceModel) All() (articles []*ExportService, err error) {
	dbase := db.GetDB()
	rows, err := dbase.Query("GetExportServicesList")
	if err != nil {
		return  nil ,err
	}
	defer rows.Close()

	expSrvs := make([]*ExportService,0)
	for rows.Next(){
		exp:=new(ExportService)
		err:=rows.Scan(&exp.Id,&exp.IdPred,&exp.LongName,&exp.UserId,&exp.SortList,&exp.GroupLIst,&exp.ShortName,&exp.NodmGroups,&exp.NodmGroupSort,&exp.GroupNumber)
		if err != nil {
			return  nil ,err
		}
		expSrvs=append(expSrvs,exp)
	}
	if err = rows.Err(); err != nil {
		return  nil ,err
	}
	return expSrvs, nil
}

func (m ExportServiceModel) One(id int) ( *ExportService,  error) {
	dbase := db.GetDB()
	rows := dbase.QueryRow("exec [GetExportServicesListOneItem] ?", id)
	exp:=new(ExportService)
	err:=rows.Scan(
		&exp.Id,
		&exp.IdPred,
		&exp.LongName,
		&exp.UserId,
		&exp.SortList,
		&exp.GroupLIst,
		&exp.ShortName,
		&exp.NodmGroups,
		&exp.NodmGroupSort,
		&exp.GroupNumber,
		)
	if err == sql.ErrNoRows {
		return  nil ,nil
	} else if err != nil {
		return  nil ,err
	}
	return exp, nil
}
