package models

import (
	"github.com/say/apisql2/db"
//	"database/sql"

	"regexp"
	"strconv"
)

//Article ...
//type Article struct {
//	ID        int64    `db:"id, primarykey, autoincrement" json:"id"`
//	UserID    int64    `db:"user_id" json:"-"`
//	Title     string   `db:"title" json:"title"`
//	Content   string   `db:"content" json:"content"`
//	UpdatedAt int64    `db:"updated_at" json:"updated_at"`
//	CreatedAt int64    `db:"created_at" json:"created_at"`
//	User      *JSONRaw `db:"user" json:"user"`
//}
type ExportServiceValues struct {
	Id int
	Idd int
	IdPred int       `json:"idpred,omitempty"`
	IdMes int        `json:"idmes,omitempty"`
	IdYear int       `json:"idyear,omitempty"`
	Value float32    `json:"value,omitempty"`
	Plan float32     `json:"plan,omitempty"`
}
func (ex ExportServiceValues) IsCorrect() bool {
	ye  := regexp.MustCompile(`(20[0-9]{2})`)
	mo  := regexp.MustCompile(`(0?[1-9]|1[012])$`)
	if ye.MatchString(strconv.Itoa(ex.IdYear)) && mo.MatchString(strconv.Itoa(ex.IdMes)) {
		return true
	}
	return false
}

//ArticleModel ...
type ExportServiceValuesModel struct{}

//All ...
func (m ExportServiceValuesModel) All() ([]*ExportServiceValues, error) {
	dbase := db.GetDB()
	rows, err := dbase.Query("GetExportServicesValues")
	if err != nil {
		return  nil ,err
	}
	defer rows.Close()
	expSrvs := make([]*ExportServiceValues,0)
	for rows.Next(){
		exp:=new(ExportServiceValues)
		err:=rows.Scan(&exp.Id,&exp.Idd,&exp.IdPred,&exp.IdMes,&exp.IdYear,&exp.Value,&exp.Plan)
		if err != nil {
			return  nil ,err
		}
		expSrvs=append(expSrvs,exp)
	}
	if err = rows.Err(); err != nil {
		return  nil ,err
	}
	return expSrvs, nil
}

func (m ExportServiceValuesModel) AllYear(year int) ([]*ExportServiceValues, error) {
	dbase := db.GetDB()
	rows, err := dbase.Query("exec [GetExportServicesValuesOneYear] ?", year)
	if err != nil {
		return  nil ,err
	}
	defer rows.Close()
	expSrvs := make([]*ExportServiceValues,0)
	for rows.Next(){
		exp:=new(ExportServiceValues)
		err:=rows.Scan(&exp.Id,&exp.Idd,&exp.IdPred,&exp.IdMes,&exp.IdYear,&exp.Value,&exp.Plan)
		if err != nil {
			return  nil ,err
		}
		expSrvs=append(expSrvs,exp)
	}
	if err = rows.Err(); err != nil {
		return  nil ,err
	}
	return expSrvs, nil
}

func (m ExportServiceValuesModel) AllMonth(year int, month int) ([]*ExportServiceValues, error) {
	dbase := db.GetDB()
	rows, err := dbase.Query("exec [GetExportServicesValuesOneYearMonth] ?,?", year,month)

	if err != nil {
		return  nil ,err
	}
	defer rows.Close()
	expSrvs := make([]*ExportServiceValues,0)
	for rows.Next(){
		exp:=new(ExportServiceValues)
		err:=rows.Scan(&exp.Id,&exp.IdPred,&exp.IdMes,&exp.IdYear,&exp.Value,&exp.Plan)
		if err != nil {
			return  nil ,err
		}
		expSrvs=append(expSrvs,exp)
	}
	if err = rows.Err(); err != nil {
		return  nil ,err
	}
	return expSrvs, nil
}

func (m ExportServiceValuesModel) AllMonthPred(year int, month int, pred int) ([]*ExportServiceValues, error) {
	dbase := db.GetDB()
	rows, err := dbase.Query("exec [GetExportServicesValuesOneByYearMonthPred] ?,?,?", pred,year,month)

	if err != nil {
		return  nil ,err
	}
	defer rows.Close()
	expSrvs := make([]*ExportServiceValues,0)
	for rows.Next(){
		exp:=new(ExportServiceValues)
		err:=rows.Scan(&exp.Id,&exp.IdPred,&exp.IdMes,&exp.IdYear,&exp.Value,&exp.Plan)
		if err != nil {
			return  nil ,err
		}
		expSrvs=append(expSrvs,exp)
	}
	if err = rows.Err(); err != nil {
		return  nil ,err
	}
	return expSrvs, nil
}
func (m ExportServiceValuesModel) AddEditExportServicesValues(ex ExportServiceValues) (bool, string) {
	res, err := db.GetDB().Exec("exec [GetExportServicesValuesUpdateItem] ?,?,?,?,?", ex.IdPred,ex.IdYear,ex.IdMes,ex.Value,ex.Plan)
	if err != nil {
		return  false ,err.Error()
	}
	count, err := res.RowsAffected()
	if err != nil {
		return  false ,err.Error()
	}else if count <1 {
		return  false ,"No records updated on SQl server"
	}
	return true, "OK"

}