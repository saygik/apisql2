package db

import (
	"database/sql"
	"log"

	//"github.com/go-gorp/gorp"
	//_ "github.com/lib/pq" //import postgres
	_ "github.com/denisenkom/go-mssqldb"
	//"github.com/say/testapi/databases"
	//"github.com/go-gorp/gorp"
	"fmt"
//	"github.com/go-gorp/gorp"
)

//DB ...
type DB struct {
	*sql.DB
}

const (
	//DbServer ...
	DbServer = "ivc-sql16.brnv.rw"
	//DbUser ...
	DbUser = "web"
	//DbPassword ...
	DbPassword = "bew"
	//DbName ...
	DbName = "Bank"
)

//var db *gorp.DbMap
var db *sql.DB
//Init ...
func Init() {

	dbinfo := fmt.Sprintf("server=%s;database=%s;user id=%s;password=%s", DbServer,DbName,DbUser,DbPassword)
	var err error
	db, err = ConnectDB(dbinfo)
	if err != nil {
		log.Fatal(err)
	}
}

//ConnectDB ...
func ConnectDB(dataSourceName string) (*sql.DB, error) {
	db, err := sql.Open("mssql", dataSourceName)
	if err != nil {
		return nil, err
	}
	if err = db.Ping(); err != nil {
		return nil, err
	}
	//dbmap.TraceOn("[gorp]", log.New(os.Stdout, "golang-gin:", log.Lmicroseconds)) //Trace database requests
	return db, nil
}

//GetDB ...
func GetDB() *sql.DB {
	return db
}
