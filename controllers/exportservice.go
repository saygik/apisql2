package controllers

import (
	"github.com/say/apisql2/models"
	"github.com/gin-gonic/gin"
	"strconv"
	"fmt"
)

//ArticleController ...
type ExportServiceController struct{}

var exportsServiceModel = new(models.ExportServiceModel)


//All ...
func (ctrl ExportServiceController) All(c *gin.Context) {

	data, err := exportsServiceModel.All()

	if err != nil {
		c.JSON(406, gin.H{"Message": "Could not get the Export Services", "error": err.Error()})
		c.Abort()
		return
	}

	c.JSON(200, gin.H{"data": data})
}

func (ctrl ExportServiceController) One(c *gin.Context) {
	if id, err := strconv.Atoi(c.Param("id")); err == nil {
		data, err := exportsServiceModel.One(id)
		if err != nil {
			c.JSON(404, gin.H{"Message": "ExportService not found", "error": err.Error()})
			c.Abort()
			return
		} else if data==nil {
			fmt.Println(data)
			c.JSON(404, gin.H{"Message": "ExportService not found"})
			c.Abort()
			return
		}
		c.JSON(200, gin.H{"data": data})
	} else {
		c.JSON(404, gin.H{"Message": "Invalid parameter", "error": err.Error()})
	}
}

