package controllers

import (
	"regexp"
	"strconv"

)

type regexParams struct {
	year string
	month string
	empty string
}

var RegexParams regexParams
func Init() {
	RegexParams=regexParams{year:`(20[0-9]{2})`,month:`(0?[1-9]|1[012])$`,empty:""}
}
func isCorrectParam (str string,regex string) (int, bool) {
	r  := regexp.MustCompile(regex)
	if ss:=r.MatchString(str); ss {
		if nyear, err := strconv.Atoi(str); err==nil {
			return nyear, true
		}
	}
	return 0, false
}
