package controllers

import (
	"github.com/say/apisql2/models"
	"github.com/gin-gonic/gin"
	"fmt"

)

//ArticleController ...
type ExportServiceValuesController struct{}

var exportsServiceValuesModel = new(models.ExportServiceValuesModel)


//All ...
func (ctrl ExportServiceValuesController) All(c *gin.Context) {

	data, err := exportsServiceValuesModel.All()
	if err != nil {
		c.JSON(406, gin.H{"Message": "Could not get the Export Service Values", "error": err.Error()})
		c.Abort()
		return
	}

	c.JSON(200, gin.H{"data": data})
}

//Add ...
func (ctrl ExportServiceValuesController) Add(c *gin.Context) {
	var  exp models.ExportServiceValues
	c.BindJSON(&exp)
	if exp.IsCorrect(){
		fmt.Println("Values updated:")
		fmt.Println(exp)
		if res,errstring:=exportsServiceValuesModel.AddEditExportServicesValues(exp);res {
			c.JSON(200, "Values updated")
		} else {
			c.JSON(400, gin.H{"Message":errstring})
		}
	}else {
		c.JSON(304, gin.H{"Message":fmt.Sprintf("Invalid data: "), "Data": exp})
	}

}
//All in one year ...
func (ctrl ExportServiceValuesController) AllYear(c *gin.Context) {
	if 	year,isCorrect:=isCorrectParam(c.Param("year"), RegexParams.year);isCorrect {
			data, err := exportsServiceValuesModel.AllYear(year)
			if err != nil {
				c.JSON( 204, gin.H{"Message": fmt.Sprintf("Could not get the Export Service Values by this year: %s", year)})
			} else {
				c.JSON(200, gin.H{"data": data})
			}
	} else {
		c.JSON(400, gin.H{"Message":fmt.Sprintf("Invalid parameter year: %s",c.Param("year"))})
		}
}


func (ctrl ExportServiceValuesController) AllMonth(c *gin.Context) {
	year,isCorrect1:=isCorrectParam(c.Param("year"),RegexParams.year)
	month,isCorrect2:=isCorrectParam(c.Param("month"),RegexParams.month)

	if isCorrect1 && isCorrect2  {
			data, err := exportsServiceValuesModel.AllMonth(year,month)
			if err != nil {
				c.JSON( 400, gin.H{"Message": fmt.Sprintf("Could not get the Export Service Values by this year: %s and month %s", year, month),"Error":err.Error()})
			} else {
				c.JSON(200, gin.H{"data": data})
			}
	} else {
		c.JSON( 400, gin.H{"Message": fmt.Sprintf("Invalid parameter : year %s or month %s ", c.Param("year"), c.Param("month"))})
	}
}

func (ctrl ExportServiceValuesController) AllMonthPred(c *gin.Context) {
	year,isCorrect1:=isCorrectParam(c.Param("year"),RegexParams.year)
	month,isCorrect2:=isCorrectParam(c.Param("month"),RegexParams.month)
	pred,isCorrect3:=isCorrectParam(c.Param("pred"),RegexParams.empty)

	if isCorrect1 && isCorrect2 && isCorrect3  {
			data, err := exportsServiceValuesModel.AllMonthPred(year,month,pred)
			if err != nil {
				c.JSON( 400, gin.H{"Message": fmt.Sprintf("Could not get the Export Service Values by this year: %s and month %s and pred %s", year, month, pred),"Error":err.Error()})
			} else {
				c.JSON(200, gin.H{"data": data})
			}
		}else {
		c.JSON( 400, gin.H{"Message": fmt.Sprintf("Invalid parameter : year %s or month %s or pred %s", c.Param("year"), c.Param("month"), c.Param("pred"))})
		}
}


//func (ctrl exportsServiceValuesModel) One(c *gin.Context) {
//	if id, err := strconv.Atoi(c.Param("id")); err == nil {
//		data, err := exportsServiceModel.One(id)
//		if err != nil {
//			c.JSON(404, gin.H{"Message": "ExportService not found", "error": err.Error()})
//			c.Abort()
//			return
//		} else if data==nil {
//			fmt.Println(data)
//			c.JSON(404, gin.H{"Message": "ExportService not found"})
//			c.Abort()
//			return
//		}
//		c.JSON(200, gin.H{"data": data})
//	} else {
//		c.JSON(404, gin.H{"Message": "Invalid parameter", "error": err.Error()})
//	}
//}

